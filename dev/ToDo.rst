- Framework

  - PyiUpdater

    - More logging
    - move data.json, keys.db & version-meta.db to pyiuconfig.db

  - Package Handler

    - check make patch | better logging

  - Test Suite

    - Focus on client auto update tests
    - Cli needs coverage
    - Add more tests!
    - Coverage is lacking

  - Client

    - Check manifest download for duplicate downloading
